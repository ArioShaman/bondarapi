# encoding: utf-8

class AdminMailer < ApplicationMailer
    default from: 'ArioShaman69@yandex.ru'


    def send_order_mail(orders, order_data)
        @order_data = order_data
        @orders = orders
        subject = 'Новый заказ'     
        @from = 'ariosha6@gmail.com'
        @to = ['kreinin98@mail.ru', 'vdv.bondarev@yandex.ru']
        @host = "http://83.220.175.223:5000"

        mail(
            from: @from,
            to: @to,
            subject: 'Подтверждение нового заказа',
            locals: {
                orders: @orders,
                order_data: @order_data,
                host: @host
            }
        )       
    end
end
