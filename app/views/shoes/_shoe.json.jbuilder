json.extract!  shoe, 
    :id, 
    :name,
    :price,
    :priceSale,
    :isSale,
    :isNew,
    :sale,
    :popular,
    :rating,
    :desc,
    :params,
    :created_at

json.type shoe.type.name
json.colors shoe.colors.collect { |color| color.name }
json.sizes shoe.sizes.collect { |size| size.name }
json.images shoe.images.collect { |image| image.image.url }