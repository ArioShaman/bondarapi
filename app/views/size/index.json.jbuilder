json.array! @sizes do |size|
    json.id size.id
    json.name size.name
    json.active  false
end