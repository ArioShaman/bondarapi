json.array! @colors do |color|  
    json.id color.id
    json.name color.name
    json.active  false
end