class ImagesController < ApplicationController
    before_action :authenticate_user!, only: [:new, :create, :destroy, :update]
    
    def create
        @image = Image.new(
            image: params[:image]
        )
        if @image.save
            puts ">>>>>>>>>>>>... #{params[:shoe_id]}"
            if params[:shoe_id]
                ShoeImage.create(shoe_id: params[:shoe_id], image_id: @image.id)
            end
            render json: @image, status: :created
        else
            render json: @image.errors, status: :unprocessable_entity
        end           
    end

    def destroy
        Image.all.each do |i|
            if i.image.url == params[:image]
                ShoeImage.where(image_id: i.id, shoe_id: params[:shoe_id]).first.destroy
                Image.find(i.id).destroy
            end 
        end
    end
end
