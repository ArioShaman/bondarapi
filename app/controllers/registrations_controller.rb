class RegistrationsController < Devise::RegistrationsController

    def create
        @user = User.new(
                email: params[:email], 
                name: params[:name], 
                last_name: params[:last_name], 
                password: params[:password])
        if @user.save 
            render json: @user, status: :created
        else
            render json: {msg: 'not registered'}, status: :bad_request
        end
    end

    private


end