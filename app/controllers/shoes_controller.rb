class ShoesController < ApplicationController
    before_action :set_shoe, only: [:show, :destroy]
    before_action :authenticate_user!, only: [:new, :create, :destroy, :update]
    
    def index
        @shoes = Shoe.all
        @shoes
    end
    
    def show
        @shoe
    end

    def indexes
        @shoes = Shoe.all.order(created_at: :desc)
        @shoes
    end


    def create
        begin
            shoeParams = {}
            params[:params].each { |param|  
                shoeParams[param[:key]] = param[:value]
            }
            priceSale = 0

            if params[:isSale]
                priceSale = params[:price].to_i - params[:sale].to_f/100 * params[:price].to_i
            end
            @shoe = Shoe.create(
                name: params[:title],
                price: params[:price],
                type_id: params[:type_id],
                desc: params[:description],
                params: shoeParams,
                popular: params[:isPopular],
                isSale: params[:isSale],
                isNew: params[:isNew],
                priceSale: priceSale,
                sale: params[:sale]
            )
            shoeId = @shoe.id
            params[:colors].each { |color|
                if color[:active] == true
                    if !ShoeColor.where(shoe_id: shoeId, color_id: color[:id]).exists?
                        ShoeColor.create(
                            shoe_id: shoeId,
                            color_id: color[:id]
                        )
                    end
                end
            }
            params[:sizes].each { |size| 
                if size[:active] == true
                    if !ShoeSize.where(shoe_id: shoeId, size_id: size[:id]).exists?
                        ShoeSize.create(
                            shoe_id: shoeId,
                            size_id: size[:id]
                        )
                    end
                end            
            }
            params[:images].each { |img|  
                ShoeImage.create(
                    shoe_id: shoeId,
                    image_id: img[:id]
                )
            }
            render json: {status: :ok}
        rescue
            render json: @shoe.error 
        end
    end

    def update
        begin

            priceSale = 0

            if params[:isSale]
                priceSale = params[:price].to_i - params[:sale].to_f/100 * params[:price].to_i
            end

            Shoe.find(params[:id]).update(
                name: params[:title],
                price: params[:price],
                type_id: params[:type_id],
                desc: params[:description],
                params: params[:params],
                popular: params[:isPopular],
                isSale: params[:isSale],
                isNew: params[:isNew],
                priceSale: priceSale,
                sale: params[:sale]
            )
            shoeId = params[:id]
            params[:colors].each { |color|
                if color[:active] == true
                    # Добавление цвета
                    if !ShoeColor.where(shoe_id: shoeId, color_id: color[:id]).exists?
                        ShoeColor.create(
                            shoe_id: shoeId,
                            color_id: color[:id]
                        )
                    end
                else
                    # Удаление цвета
                    if ShoeColor.where(shoe_id: shoeId, color_id: color[:id]).exists?
                        ShoeColor.where(shoe_id: shoeId, color_id: color[:id]).first.destroy
                    end
                end
            }
            params[:sizes].each { |size| 
                if size[:active] == true
                    if !ShoeSize.where(shoe_id: shoeId, size_id: size[:id]).exists?
                        ShoeSize.create(
                            shoe_id: shoeId,
                            size_id: size[:id]
                        )
                    end
                else
                    if ShoeSize.where(shoe_id: shoeId, size_id: size[:id]).exists?
                        ShoeSize.where(shoe_id: shoeId, size_id: size[:id]).first.destroy
                    end
                end            
            }
            render json: {status: :ok}
        rescue
            render json: @shoe.error 
        end        
    end

    def destroy
        @shoe.destroy
        render json: {status: :ok}
    end

    private
        def set_shoe
            @shoe = Shoe.find(params[:id])
        end
end
