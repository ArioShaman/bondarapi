class Shoe < ApplicationRecord
    belongs_to :type
    has_many :shoe_colors
    has_many :colors, through: :shoe_colors

    has_many :shoe_sizes
    has_many :sizes, through: :shoe_sizes   

    has_many :shoe_images
    has_many :images, through: :shoe_images        
end
