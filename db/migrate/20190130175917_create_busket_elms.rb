class CreateBusketElms < ActiveRecord::Migration[5.2]
    def up
        create_table :busket_elms do |t|
            t.integer :count, presence: true
            t.json :object, presence: true     

            t.timestamps
        end   
    end
    def down
        drop_table :busket_elms
    end
end


