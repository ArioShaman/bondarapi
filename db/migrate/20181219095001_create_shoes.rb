class CreateShoes < ActiveRecord::Migration[5.2]
    def up
        create_table :shoes do |t|
            t.string :name, presence: true 
            t.integer :sale, default: 0

            t.decimal :price, presence: true, precision: 10, scale: 2
            t.decimal :priceSale, presence: true, precision: 10, scale: 2

            t.boolean :isSale, default: false
            t.boolean :isNew, default: false
            t.boolean :popular, default: false
            t.integer :rating, default: 0

            t.belongs_to :type, index: true
            t.timestamps
        end
    end

    def down
        drop_table :shoes
    end
end
