class CreateImages < ActiveRecord::Migration[5.2]
    def up
        create_table :images do |t|
            t.string :image, presence: true
            t.timestamps
        end
    end

    def down
        drop_table :images
    end
end
