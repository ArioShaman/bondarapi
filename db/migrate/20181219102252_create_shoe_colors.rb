class CreateShoeColors < ActiveRecord::Migration[5.2]
    def up
        create_table :shoe_colors do |t|
            t.belongs_to :shoe, index: true
            t.belongs_to :color, index: true

            t.timestamps
        end
    end

    def down
        drop_table :shoe_colors
    end  
end