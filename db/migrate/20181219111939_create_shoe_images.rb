class CreateShoeImages < ActiveRecord::Migration[5.2]
    def up
        create_table :shoe_images do |t|
            t.belongs_to :shoe, index: true
            t.belongs_to :image, index: true
            t.timestamps
        end
    end

    def down
        drop_table :shoe_images
    end
end
