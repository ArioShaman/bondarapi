class CreateColors < ActiveRecord::Migration[5.2]
    def change
        create_table :colors do |t|
            t.string :name, unique: true, presence: true 
            t.timestamps
        end
    end
end
