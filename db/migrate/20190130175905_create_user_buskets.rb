class CreateUserBuskets < ActiveRecord::Migration[5.2]
    def up
        create_table :user_buskets do |t|
            t.belongs_to :users, index: true
            t.belongs_to :busket_elms, index: true 

            t.timestamps
        end
    end
    def down
        drop_table :user_buskets
    end
end