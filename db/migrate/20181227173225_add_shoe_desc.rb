class AddShoeDesc < ActiveRecord::Migration[5.2]
    def change
        add_column :shoes, :desc, :text
        add_column :shoes, :params, :json
    end
end
