class CreateShoeSizes < ActiveRecord::Migration[5.2]
    def up
        create_table :shoe_sizes do |t|
            t.belongs_to :shoe, index: true
            t.belongs_to :size, index: true

            t.timestamps
        end
    end

    def down
        drop_table :shoe_sizes
    end
end
