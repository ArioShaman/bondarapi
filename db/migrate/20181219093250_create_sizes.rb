class CreateSizes < ActiveRecord::Migration[5.2]
    def up
        create_table :sizes do |t|
            t.string :name, unique: true, presence: true 
            t.timestamps
        end
    end

    def down
        raise ActiveRecord::IrreversibleMigration
    end
end
