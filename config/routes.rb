Rails.application.routes.draw do
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    
    scope '/api' do
        mount_devise_token_auth_for 'User', at: 'auth', controllers: { 
            registrations: 'registrations' 
        }
        resources :shoes, :defaults => { :format => 'json' } do

        end
        resources :color,   :defaults => { :format => 'json' }, :only => [:index]
        resources :size,    :defaults => { :format => 'json' }, :only => [:index]
        resources :images,  :defaults => { :format => 'json' }, :only => [:create] 
        
        post 'images/delete', to: 'images#destroy', :defaults => { :format => 'json' }

        get 'indexes', to: 'shoes#indexes', :defaults => { :format => 'json' }
        post 'order', to: 'user_buskets#order', :defaults => { :format => 'json' }
    end
end
